import React from 'react';
import ButtonComponent from '../components/ui/ButtonComponent.js';
import SwitchComponent from '../components/ui/Switch.Component.js';
import WrapperScreen from './WrapperScreen.js';

const UiComponentsScreen = () => {
  return (
    <WrapperScreen>
      <ButtonComponent />
      <SwitchComponent />
    </WrapperScreen>
  );
};

export default UiComponentsScreen;
