import React from 'react';
import FlatListComponent from '../components/list/FlatListComponent.js';
import SectionListComponent from '../components/list/SectionListComponent.js';
import WrapperScreen from './WrapperScreen.js';

const ListComponentsScreen = () => {
  return (
    <WrapperScreen>
      <FlatListComponent />
      <SectionListComponent />
    </WrapperScreen>
  );
};

export default ListComponentsScreen;
