import React from 'react';
import TextComponent from '../components/basic/TextComponent.js';
import InputComponent from '../components/basic/InputComponent.js';
import ImageComponent from '../components/basic/ImageComponent.js';
import ScrollViewComponent from '../components/basic/ScrollViewComponent.js';
import WrapperScreen from './WrapperScreen.js';

const BasicComponentsScreen = () => {
  return (
    <WrapperScreen>
      <TextComponent />
      <InputComponent />
      <ImageComponent />
      <ScrollViewComponent />
    </WrapperScreen>
  );
};

export default BasicComponentsScreen;
