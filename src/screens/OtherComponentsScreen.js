import React from 'react';
import ActivityIndicatorComponent from '../components/others/ActivityIndicatorComponent.js';
import ModalComponent from '../components/others/ModalComponent.js';
import WrapperScreen from './WrapperScreen.js';

const OtherComponentsScreen = () => {
  return (
    <WrapperScreen>
      <ActivityIndicatorComponent />
      <ModalComponent />
    </WrapperScreen>
  );
};

export default OtherComponentsScreen;
