import React from 'react';
import { View, Text, FlatList, StyleSheet, Dimensions } from 'react-native';

const FlatListComponent = () => {
  const data = [
    {
      id: 1,
      value: 'Charan',
    },
    {
      id: 2,
      value: 'Abhinav',
    },
    {
      id: 3,
      value: 'Hari',
    },
    {
      id: 4,
      value: 'Preetham',
    },
  ];

  const ListItem = ({ item }) => (
    <View style={styles.listItem}>
      <Text style={styles.listText}>Name: {item.value}</Text>
    </View>
  );

  return (
    <FlatList
      data={data}
      renderItem={ListItem}
      keyExtractor={item => item.id}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: 'gray',
    padding: 10,
    marginTop: 5,
    width: Dimensions.get('screen').width / 2,
  },
  listText: {
    color: 'white',
  },
});

export default FlatListComponent;
