import React from 'react';
import { View, Text, SectionList, StyleSheet, Dimensions } from 'react-native';

const FlatListComponent = () => {
  const data = [
    {
      title: 'Main dishes',
      data: ['Pizza', 'Burger', 'Risotto'],
    },
    {
      title: 'Sides',
      data: ['French Fries', 'Onion Rings', 'Fried Shrimps'],
    },
    {
      title: 'Drinks',
      data: ['Water', 'Coke', 'Beer'],
    },
    {
      title: 'Desserts',
      data: ['Cheese Cake', 'Ice Cream'],
    },
  ];

  const ListItem = ({ title }) => (
    <View style={styles.listItem}>
      <Text style={styles.listText}>{title}</Text>
    </View>
  );

  return (
    <SectionList
      style={styles.sectionList}
      sections={data}
      keyExtractor={(item, index) => item + index}
      renderItem={({ item }) => <ListItem title={item} />}
      renderSectionHeader={({ section: { title } }) => (
        <Text style={styles.sectionHeader}>{title}</Text>
      )}
    />
  );
};

const styles = StyleSheet.create({
  sectionList: {
    marginTop: 10,
  },
  listItem: {
    backgroundColor: 'gray',
    padding: 10,
    marginTop: 5,
    width: Dimensions.get('screen').width / 2,
  },
  sectionHeader: {
    marginTop: 5,
    fontSize: 20,
    color: 'brown',
  },
  listText: {
    color: 'white',
  },
});

export default FlatListComponent;
