import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const ImageComponent = () => {
  return (
    <View>
      {/* dimensions are not loaded automatically for images from the web */}
      <Image
        source={{
          uri: 'https://www.yourdictionary.com/images/definitions/lg/12337.summit.jpg',
        }}
        style={styles.img}
      />
      {/* dimensions are loaded automatically for local images*/}
      <Image source={require('../../assets/summit.jpg')} />
    </View>
  );
};

const styles = StyleSheet.create({
  img: {
    marginTop: 20,
    height: 100,
    width: 100,
  },
});

export default ImageComponent;
