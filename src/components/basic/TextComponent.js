import React from 'react';
import { Text, StyleSheet } from 'react-native';

const TextComponent = () => {
  return <Text style={styles.test}>Hello</Text>;
};

const styles = StyleSheet.create({
  test: {
    color: 'red',
  },
});

export default TextComponent;
