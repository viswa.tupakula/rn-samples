import React, { useState } from 'react';
import { TextInput, StyleSheet } from 'react-native';

const InputComponent = () => {
  const [name, setName] = useState();
  const onChangeText = text => {
    console.log(text);
    setName(text);
  };

  return (
    <TextInput
      style={styles.input}
      onChangeText={onChangeText}
      value={name}
      placeholder="Enter Name"
    />
  );
};

const styles = StyleSheet.create({
  input: {
    marginTop: 10,
    width: 150,
    borderWidth: 1,
    padding: 5,
  },
});

export default InputComponent;
