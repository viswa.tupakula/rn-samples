import React from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions } from 'react-native';

const ScrollViewComponent = () => {
  return (
    <View style={styles.scrollContainer}>
      <ScrollView fadingEdgeLength={30}>
        <Text>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni fuga,
          labore nemo voluptate autem molestiae minus, tempore quisquam
          voluptatem harum temporibus, dolorem accusamus laboriosam blanditiis
          libero fugiat quis esse vero asdag fsd gsd gsdgsdge s swre gsegsegs
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni fuga,
          labore nemo voluptate autem molestiae minus, tempore quisquam
          voluptatem harum temporibus, dolorem accusamus laboriosam blanditiis
          libero fugiat quis esse vero.
        </Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    padding: 5,
    marginTop: 5,
    borderWidth: 2,
    height: Dimensions.get('screen').height / 10,
  },
});

export default ScrollViewComponent;
