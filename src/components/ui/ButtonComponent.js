import React from 'react';
import {
  Text,
  View,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  Pressable,
  StyleSheet,
} from 'react-native';

const ButtonComponent = () => {
  return (
    <View>
      <Button
        title="RN Button"
        color="#841584"
        onPress={() => {
          console.log('Button Pressed');
        }}
      />
      <TouchableOpacity
        style={styles.tOpacity}
        onPress={() => {
          console.log('Touchable Opacity Pressed');
        }}
      >
        <Text style={styles.btnTxt}>Touchable Opacity</Text>
      </TouchableOpacity>
      <TouchableHighlight
        style={styles.tOpacity}
        onPress={() => {
          console.log('Touchable Highlight Pressed');
        }}
      >
        <Text>Touchable Highlight</Text>
      </TouchableHighlight>
      <Pressable
        style={styles.tOpacity}
        onPressIn={() => {
          console.log('Pressed in');
        }}
        onLongPress={() => {
          console.log('Long Press');
        }}
        onPressOut={() => {
          console.log('Pressed out');
        }}
      >
        <Text>Pressable</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  tOpacity: {
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#a6c',
    marginTop: 10,
  },
  btnTxt: {
    color: 'white',
    fontSize: 20,
  },
});

export default ButtonComponent;
