import React from 'react';
import { ActivityIndicator, Text, View } from 'react-native';

const ActivityIndicatorComponent = () => {
  return (
    <View>
      <Text>Activity Indicators</Text>
      <ActivityIndicator />
      <ActivityIndicator size="large" />
      <ActivityIndicator size="small" color="#0000ff" />
      <ActivityIndicator size="large" color="#00ff00" />
    </View>
  );
};

export default ActivityIndicatorComponent;
