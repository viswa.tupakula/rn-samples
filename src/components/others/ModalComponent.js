import React, { useState } from 'react';
import { Text, View, Modal, Button, StyleSheet } from 'react-native';

const ModalComponent = () => {
  const [visible, setVisible] = useState(false);

  return (
    <View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
          setVisible(!visible);
        }}
      >
        <View style={styles.modalContent}>
          <Text>Modal is open</Text>
          <Button title="Close Modal" onPress={() => setVisible(false)} />
        </View>
      </Modal>
      <Button title="Open Modal" onPress={() => setVisible(true)} />
    </View>
  );
};

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    backgroundColor: 'lightblue',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ModalComponent;
