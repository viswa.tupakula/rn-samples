import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import BasicComponentsScreen from '../screens/BasicComponentsScreen.js';
import UiComponentsScreen from '../screens/UiComponentsScreen.js';
import ListComponentsScreen from '../screens/ListComponentsScreen.js';
import OtherComponentsScreen from '../screens/OtherComponentsScreen.js';
import Ionicons from 'react-native-vector-icons/Ionicons';

const AppNavigator = () => {
  const Tab = createBottomTabNavigator();
  const tabIcons = (route, focused, color, size) => {
    let iconName;
    switch (route.name) {
      case 'Basic':
        iconName = 'ios-flame';
        break;
      case 'UI':
        iconName = 'ios-ribbon';
        break;
      case 'List':
        iconName = 'list';
        break;
      default:
        iconName = 'md-sync';
    }
    return <Ionicons name={iconName} size={size} color={color} />;
  };

  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) =>
            tabIcons(route, focused, color, size),
          tabBarActiveTintColor: 'red',
          tabBarActiveBackgroundColor: 'lightgray',
          tabBarInactiveTintColor: 'gray',
          tabBarLabelStyle: {
            fontSize: 12,
          },
        })}
      >
        <Tab.Screen name="Basic" component={BasicComponentsScreen} />
        <Tab.Screen name="UI" component={UiComponentsScreen} />
        <Tab.Screen name="List" component={ListComponentsScreen} />
        <Tab.Screen name="Others" component={OtherComponentsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
